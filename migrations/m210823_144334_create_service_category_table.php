<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_144334_create_service_category_table`.
 */
class m210823_144334_create_service_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('service_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('service_category');
    }
}
