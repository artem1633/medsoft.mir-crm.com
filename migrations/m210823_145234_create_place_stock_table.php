<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_145234_create_place_stock_table`.
 */
class m210823_145234_create_place_stock_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('place_stock', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);

        

$this->insert('place_stock', [
            'name' => 'Полка слева ',
        ]);
$this->insert('place_stock', [
            'name' => 'Полка справа ',
        ]);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('place_stock');
    }
}
