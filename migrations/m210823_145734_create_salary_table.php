<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_145734_create_salary_table`.
 */
class m210823_145734_create_salary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('salary', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'amounts' => $this->double()->comment('Сумма'),
            'create_at' => $this->datetime()->comment('Дата и время'),
            'way' => $this->string()->comment('Способ'),
            'status' => $this->string()->comment('Статус'),
            'branch_id' => $this->integer()->comment('Филиал'),
        ]);

        $this->createIndex(
            'idx-salary-user_id',
            'salary',
            'user_id'
        );
                        
        $this->addForeignKey(
            'fk-salary-user_id',
            'salary',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-salary-branch_id',
            'salary',
            'branch_id'
        );
                        
        $this->addForeignKey(
            'fk-salary-branch_id',
            'salary',
            'branch_id',
            'branches',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-salary-user_id',
            'salary'
        );
                        
        $this->dropIndex(
            'idx-salary-user_id',
            'salary'
        );
                        
                        $this->dropForeignKey(
            'fk-salary-branch_id',
            'salary'
        );
                        
        $this->dropIndex(
            'idx-salary-branch_id',
            'salary'
        );
                        
                        
        $this->dropTable('salary');
    }
}
