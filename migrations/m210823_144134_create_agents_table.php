<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210823_144134_create_agents_table`.
 */
class m210823_144134_create_agents_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('agents', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'phone' => $this->string()->comment('Телефон'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('agents');
    }
}
