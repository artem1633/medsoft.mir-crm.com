<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\TypesCertificates */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
	$model->zaklyuchenie = explode(',', $model->zaklyuchenie);
}
?>
<div class="types-certificates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                       
             <?= $form->field($model, 'name', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'price', ['cols' => 12, 'colsOptionsStr' => " "])->textInput(['type' => 'number'])  ?>
                    
             <?= $form->field($model, 'agentu', ['cols' => 12, 'colsOptionsStr' => " "])->textInput(['type' => 'number'])  ?>
                    
         <?= $form->field($model, 'zaklyuchenie')->widget(Select2::class, [
                'data' => [],
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'tags' => true,
                ],
        ]) ?>

                                
         <?= $form->field($model, 'shablon_qr_id', ['cols' => 12, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TemplateQr::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                        
         <?= $form->field($model, 'shablon_spravki_id', ['cols' => 12, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\HelpTemplate::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

