<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["types-certificates"."/".$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
   
 
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
         'visible'=>\app\models\TypesCertificates::isVisibleAttr('name'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'price',
        'visible' => \app\models\TypesCertificates::isVisibleAttr('price'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'agentu',
        'visible' => \app\models\TypesCertificates::isVisibleAttr('agentu'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'zaklyuchenie',
         'visible'=>\app\models\TypesCertificates::isVisibleAttr('zaklyuchenie'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'shablon_qr_id',
        'visible'=>\app\models\TypesCertificates::isVisibleAttr('shablon_qr_id'),
        'value'=>'shablonQr.name',
        'filter'=> ArrayHelper::map(\app\models\TemplateQr::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'shablon_spravki_id',
        'visible'=>\app\models\TypesCertificates::isVisibleAttr('shablon_spravki_id'),
        'value'=>'shablonSpravki.name',
        'filter'=> ArrayHelper::map(\app\models\HelpTemplate::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vozmojnost_udalyat',
         'visible'=>0,
    ],

];   

