<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["branches"."/".$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
   
 
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'logo',
         'visible'=>\app\models\Branches::isVisibleAttr('logo'),
         'content' => function($model){
          return  $model->logo ?  "<img src='/{$model->logo}' style='height: 100px; width: 100px; object-fit: contain;'>" : '';         }    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
         'visible'=>\app\models\Branches::isVisibleAttr('name'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'tel_1',
         'visible'=>\app\models\Branches::isVisibleAttr('tel_1'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'tel_2',
         'visible'=>\app\models\Branches::isVisibleAttr('tel_2'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'address',
         'visible'=>\app\models\Branches::isVisibleAttr('address'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'firm_id',
        'visible'=>\app\models\Branches::isVisibleAttr('firm_id'),
        'value'=>'firm.name',
        'filter'=> ArrayHelper::map(\app\models\Firm::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'grafik_raboty',
         'visible'=>0,
    ],

];   

