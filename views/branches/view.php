<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Branches */
?>
<div class="branches-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a class="btn btn-warning btn-xs" href="/branches/update?id=<?= $model->id?>&amp;containerPjaxReload=%23pjax-container-info-container" role="modal-remote"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'logo',
            'name',
            'tel_1',
            'tel_2',
            'address',
            'firm_id',
            'grafik_raboty',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/user/index.php", [
            'searchModel' => $userSearchModel,
            'dataProvider' => $userDataProvider,
            'additionalLinkParams' => ['User[branch_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/patient/index.php", [
            'searchModel' => $patientSearchModel,
            'dataProvider' => $patientDataProvider,
            'additionalLinkParams' => ['Patient[branche_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/help/index.php", [
            'searchModel' => $helpSearchModel,
            'dataProvider' => $helpDataProvider,
            'additionalLinkParams' => ['Help[branche_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/box-office/index.php", [
            'searchModel' => $boxOfficeSearchModel,
            'dataProvider' => $boxOfficeDataProvider,
            'additionalLinkParams' => ['BoxOffice[branch_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/salary/index.php", [
            'searchModel' => $salarySearchModel,
            'dataProvider' => $salaryDataProvider,
            'additionalLinkParams' => ['Salary[branch_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
