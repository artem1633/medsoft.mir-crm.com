<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model StoreManager */
?>
<div class="store-manager-view">
 <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'index' ? ' active' : '' ?>">
                                <a class="nav-link"  id="home-tab-fill" href="<?= Url::toRoute(['store-manager/index']) ?>" aria-controls="home-fill" aria-selected="true">Сводные данные</a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'remains' ? ' active' : '' ?>">
                                <a class="nav-link"  id="home-tab-fill" href="<?= Url::toRoute(['store-manager/remains']) ?>" aria-controls="home-fill" aria-selected="true">Остатки</a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'income' ? ' active' : '' ?>">
                                <a class="nav-link"  id="home-tab-fill" href="<?= Url::toRoute(['store-manager/income']) ?>" aria-controls="home-fill" aria-selected="true">Поступления</a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'out' ? ' active' : '' ?>">
                                <a class="nav-link"  id="home-tab-fill" href="<?= Url::toRoute(['store-manager/out']) ?>" aria-controls="home-fill" aria-selected="true">Списания</a>
                            </li>
                        </ul>
                        <div class="tab-content pt-1">
                            
                            <div class="tab-pane <?= Yii::$app->controller->action->id == 'index' ? ' active' : '' ?>" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                                <div class="row">
                                                                        
                        <?php if (Yii::$app->controller->action->id == 'index'):?>
                                <div class="col-md-12">
                                   <?= $this->render("@app/views/store-data/view.php"); ?>
                                </div>

                    <?php endif;?>
                                </div>
                            </div>
                            
                            <div class="tab-pane <?= Yii::$app->controller->action->id == 'remains' ? ' active' : '' ?>" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                                <div class="row">
                                                                        
                        <?php if (Yii::$app->controller->action->id == 'remains'):?>
                                <div class="col-md-12">
                                   <?= $this->render("@app/views/store-remains/index.php", [
                                        'searchModel' => $storeRemainsSearchModel,
                                        'dataProvider' => $storeRemainsDataProvider,
                                    ]); ?>
                                </div>

                    <?php endif;?>
                                </div>
                            </div>
                            
                            <div class="tab-pane <?= Yii::$app->controller->action->id == 'income' ? ' active' : '' ?>" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                                <div class="row">
                                                                        
                        <?php if (Yii::$app->controller->action->id == 'income'):?>
                                <div class="col-md-12">
                                   <?= $this->render("@app/views/store-in/index.php", [
                                        'searchModel' => $storeInSearchModel,
                                        'dataProvider' => $storeInDataProvider,
                                    ]); ?>
                                </div>

                    <?php endif;?>
                                </div>
                            </div>
                            
                            <div class="tab-pane <?= Yii::$app->controller->action->id == 'out' ? ' active' : '' ?>" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                                <div class="row">
                                                                        
                        <?php if (Yii::$app->controller->action->id == 'out'):?>
                                <div class="col-md-12">
                                   <?= $this->render("@app/views/store-salvage/index.php", [
                                        'searchModel' => $storeSalvageSearchModel,
                                        'dataProvider' => $storeSalvageDataProvider,
                                    ]); ?>
                                </div>

                    <?php endif;?>
                                </div>
                            </div>
   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php \yii\bootstrap\Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'class' => 'modal-big-md',
    ],
    "footer"=>"",// always need it for jquery plugin
]); \yii\bootstrap\Modal::end(); ?>

</div>
