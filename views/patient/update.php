<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Patient */
?>
<div class="patient-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
