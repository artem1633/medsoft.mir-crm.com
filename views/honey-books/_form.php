<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\HoneyBooks */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="honey-books-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                       
             <?= $form->field($model, 'name', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'data_rojdeniya', ['cols' => 12, 'colsOptionsStr' => " "])->input('date')  ?>
                    
             <?= $form->field($model, 'city', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'ulica', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'home', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'obekt', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'role', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

