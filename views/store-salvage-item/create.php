<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model StoreSalvageItem */

?>
<div class="store-salvage-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
