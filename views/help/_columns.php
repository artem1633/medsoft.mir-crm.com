<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["help"."/".$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'template' => '{update} {delete} {paper} {qr} {print}  {down}',
        'buttons' => [
           'paper' => function($url, $model, $key){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-file"></span>', 'http://medsoft.mir-crm.com/api/check/help?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'qr' => function($url, $model, $key){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-qrcode"></span>', 'http://medsoft.mir-crm.com/api/check/info?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'print' => function($url, $model, $key){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-print"></span>', 'http://medsoft.mir-crm.com/api/check/print?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
            'down' => function($url, $model, $key){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-open"></span>', 'http://medsoft.mir-crm.com/api/check/down?id='.$model->id,['target' => '_blank','data-pjax'=>'0',]);
            },
        ],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту запись?'], 
    ],      
 
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'qr',
         'visible'=>0,
         'content' => function($model){
          return "<img src='/{$model->qr}' style='height: 100px; width: 100px; object-fit: contain;'>";         }    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'patient_id',
        'visible'=>\app\models\Help::isVisibleAttr('patient_id'),
        'value'=>'patient.name',
        'filter'=> ArrayHelper::map(\app\models\Patient::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type_id',
        'visible'=>\app\models\Help::isVisibleAttr('type_id'),
        'value'=>'type.name',
        'filter'=> ArrayHelper::map(\app\models\TypesCertificates::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
          'convertFormat'=>true,
          'pluginEvents' => [
              'cancel.daterangepicker'=>'function(ev, picker) {}'
           ],
           'pluginOptions' => [
              'opens'=>'right',
              'locale' => [
                  'cancelLabel' => 'Clear',
                  'format' => 'Y-m-d',
               ]
           ]
         ],
        'attribute'=>'create_at',
        'visible'=>\app\models\Help::isVisibleAttr('create_at'),
        'format'=> ['date', 'php:d.m.Y H:i'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible'=>\app\models\Help::isVisibleAttr('user_id'),
        'value'=>'user.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'agent_id',
        'visible'=>\app\models\Help::isVisibleAttr('agent_id'),
        'value'=>'agent.name',
        'filter'=> ArrayHelper::map(\app\models\Agents::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'amounts',
        'visible' => \app\models\Help::isVisibleAttr('amounts'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_vzyatiya_biomateriala',
        'visible'=>0,
        'format'=> ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vremya_vzyatiya_biomateriala',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_vydachi',
        'visible'=>0,
        'format'=> ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vremya_vydachi',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vrach_id',
        'visible'=>0,
        'value'=>'vrach.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'srok',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'zaklyuchenie',
        'visible'=>0,
        'value'=>'zaklyuchenie.zaklyuchenie',
        'filter'=> ArrayHelper::map(\app\models\TypesCertificates::find()->asArray()->all(), 'id', 'zaklyuchenie'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kolichestvo',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'firm_id',
        'visible'=>\app\models\Help::isVisibleAttr('firm_id'),
        'value'=>'firm.name',
        'filter'=> ArrayHelper::map(\app\models\Firm::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'branche_id',
        'visible'=>\app\models\Help::isVisibleAttr('branche_id'),
        'value'=>'branche.name',
        'filter'=> ArrayHelper::map(\app\models\Branches::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'oplata',
         'visible'=>\app\models\Help::isVisibleAttr('oplata'),
    ],

];   

