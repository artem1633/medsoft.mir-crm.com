<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Help */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="help-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                                          
         <?= $form->field($model, 'patient_id', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Patient::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                        
         <?= $form->field($model, 'type_id', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TypesCertificates::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                        
         <?= $form->field($model, 'agent_id', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Agents::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
             <?= $form->field($model, 'amounts', ['cols' => 4, 'colsOptionsStr' => " "])->textInput(['type' => 'number'])  ?>
                    
             <?= $form->field($model, 'data_vzyatiya_biomateriala', ['cols' => 4, 'colsOptionsStr' => " "])->input('date')  ?>
                    
             <?= $form->field($model, 'vremya_vzyatiya_biomateriala', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'data_vydachi', ['cols' => 4, 'colsOptionsStr' => " "])->input('date')  ?>
                    
             <?= $form->field($model, 'vremya_vydachi', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                            
         <?= $form->field($model, 'vrach_id', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
             <?= $form->field($model, 'srok', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                            
         <?= $form->field($model, 'zaklyuchenie', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TypesCertificates::find()->all(), 'id', 'zaklyuchenie'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
             <?= $form->field($model, 'kolichestvo', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                            
         <?= $form->field($model, 'firm_id', ['cols' => 4, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Firm::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
              
             <?= $form->field($model, 'oplata')->checkbox()  ?>
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<script src="/libs/jquery.maskedinput.min.js"></script>

<script>$("#help-vremya_vzyatiya_biomateriala").mask("99:99");</script>
<script>$("#help-vremya_vydachi").mask("99:99");</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

