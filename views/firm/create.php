<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Firm */

?>
<div class="firm-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
