<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Firm */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="firm-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
                                                     
             <?= $form->field($model, 'fileLogo', ['cols' => 12, 'colsOptionsStr' => " "])->fileInput()  ?>
                    
             <?= $form->field($model, 'name', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'licenziya', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'organizaciya', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'create_at', ['cols' => 4, 'colsOptionsStr' => " "])->input('date')  ?>
                    
             <?= $form->field($model, 'srok', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'address', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'tel', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'sayt', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'license', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'date', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'name', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'adress', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'phone', ['cols' => 4, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'site', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                        
             <?= $form->field($model, 'filePechat', ['cols' => 12, 'colsOptionsStr' => " "])->fileInput()  ?>
                        
             <?= $form->field($model, 'filePechatKvitanciyu', ['cols' => 12, 'colsOptionsStr' => " "])->fileInput()  ?>
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

