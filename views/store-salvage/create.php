<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model StoreSalvage */

?>
<div class="store-salvage-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
