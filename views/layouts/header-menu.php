<?php

use app\admintheme\widgets\Menu;


?>



<div id="sidebar" class="sidebar sidebar-grid">
    <?php if (Yii::$app->user->isGuest == false): ?>        <?php        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => Yii::t('app', 'Рабочий стол'), 'icon' => 'fa fa-bar-chart', 'url' => ['/dashboard']],
                    

                        ['label' => Yii::t('app', 'Кабинет врача'), 'icon' => 'fa fa-coffee', 'url' => ['/doctor-office'], 'visible' => Yii::$app->user->identity->can('doctor_office_view')],
                        ['label' => Yii::t('app', 'Пациент'), 'icon' => 'fa fa-users', 'url' => ['/patient'], 'visible' => Yii::$app->user->identity->can('patient_view')],
                        ['label' => Yii::t('app', 'Справки'), 'icon' => 'fa fa-file-word-o', 'url' => ['/help'], 'visible' => Yii::$app->user->identity->can('help_view')],
                        ['label' => Yii::t('app', 'Мед книжки'), 'icon' => 'fa fa-book', 'url' => ['/honey-books'], 'visible' => Yii::$app->user->identity->can('honey_books_view')],
                        ['label' => Yii::t('app', 'Касса'), 'icon' => 'fa fa-money', 'url' => ['/box-office'], 'visible' => Yii::$app->user->identity->can('box_office_view')],
                        ['label' => Yii::t('app', 'Зарплата'), 'icon' => 'fa fa-universal-access', 'url' => ['/salary'], 'visible' => Yii::$app->user->identity->can('salary_view')],
                        ['label' => Yii::t('app', 'Файлы'), 'icon' => 'fa fa-file', 'url' => ['/file'], 'visible' => Yii::$app->user->identity->can('file_view')],
                        ['label' => Yii::t('app', 'Управление складом'), 'icon' => 'fa fa-balance-scale', 'url' => ['/store-manager'], 'visible' => Yii::$app->user->identity->can('store_manager_view')],
                        ['label' => Yii::t('app', 'Справочники'), 'icon' => 'fa fa-list-ul', 'url' => '#', 'options' => ['class' => 'has-sub'], 'visible' => Yii::$app->user->identity->can('books'),
                            'items' => [
    

                                ['label' => Yii::t('app', 'Агенты'), 'icon' => 'fa fa-500px', 'url' => ['/agents'], ],
                                ['label' => Yii::t('app', 'Услуги'), 'icon' => 'fa fa-500px', 'url' => ['/services'], ],
                                ['label' => Yii::t('app', 'Специализация'), 'icon' => 'fa fa-user', 'url' => ['/specialization'], ],
                                ['label' => Yii::t('app', 'Типы справок'), 'icon' => 'fa fa-500px', 'url' => ['/types-certificates'], ],
                                ['label' => Yii::t('app', 'Филиалы'), 'icon' => 'fa fa-500px', 'url' => ['/branches'], ],
                                ['label' => Yii::t('app', 'Фирмы'), 'icon' => 'fa fa-500px', 'url' => ['/firm'], ],
                                ['label' => Yii::t('app', 'Единицы измерений'), 'icon' => 'fa fa-user', 'url' => ['/ed'], ],
                                ['label' => Yii::t('app', 'Поставщики'), 'icon' => 'fa fa-apple', 'url' => ['/suppliers'], ],
                                ['label' => Yii::t('app', 'Продукты'), 'icon' => 'fa fa-apple', 'url' => ['/products'], ],
                                ['label' => Yii::t('app', 'Склад'), 'icon' => 'fa fa-archive', 'url' => ['/store'], ],
                                ['label' => Yii::t('app', 'Место на складе'), 'icon' => 'fa fa-arrows-alt', 'url' => ['/place-stock'], ],
                                ['label' => Yii::t('app', 'Категория услуг'), 'icon' => 'fa fa-500px', 'url' => ['/service-category'], ],
                                ['label' => Yii::t('app', 'Шаблон qr'), 'icon' => 'fa fa-500px', 'url' => ['/template-qr'], ],
                                ['label' => Yii::t('app', 'Шаблон справки'), 'icon' => 'fa fa-500px', 'url' => ['/help-template'], ],
                        ]],
                            
                    ['label' => Yii::t('app', 'Настройки'), 'icon' => 'fa fa-list-ul', 'url' => '#', 'options' => ['class' => 'has-sub'], 'visible' => true,
                        'items' => [
                                                        ['label' => Yii::t('app', 'Пользователи'), 'icon' => 'fa  fa-user-o', 'url' => ['/user']],
                            ['label' => Yii::t('app', 'Роли'), 'icon' => 'fa  fa-star', 'url' => ['/role']],
                            // ['label' => Yii::t('app', 'Отчеты'), 'icon' => 'fa  fa-star', 'url' => ['/report']],
                            // ['label' => Yii::t('app', 'Поля отчета'), 'icon' => 'fa  fa-star', 'url' => ['/report-column']],
                                                                            ],
                ],
                ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
