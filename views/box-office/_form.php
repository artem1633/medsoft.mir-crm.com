<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\BoxOffice */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="box-office-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                                 
         <?= $form->field($model, 'patient_id', ['cols' => 6, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Patient::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
         <?= $form->field($model, 'type', ['cols' => 6, 'colsOptionsStr' => " "])->dropDownList(app\models\BoxOffice::typeLabels(), ['prompt' => 'Выберите вариант']) ?>
      
                            
         <?= $form->field($model, 'service_id', ['cols' => 6, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Services::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
             <?= $form->field($model, 'amounts', ['cols' => 6, 'colsOptionsStr' => " "])->textInput(['type' => 'number'])  ?>
                    
         <?= $form->field($model, 'status', ['cols' => 6, 'colsOptionsStr' => " "])->dropDownList(app\models\BoxOffice::statusLabels(), ['prompt' => 'Выберите вариант']) ?>
      
                    
             <?= $form->field($model, 'comment', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

