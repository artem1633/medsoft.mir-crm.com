<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                  <div id="div-login" class="col-md-6" style=" ">        
             <?= $form->field($model, 'login')->textInput()  ?>
        </div>
        <div id="div-role" class="col-md-6" style=" ">        
             <?= $form->field($model, 'role')->textInput()  ?>
        </div>
        <div id="div-name" class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div id="div-phone" class="col-md-12" style=" ">        
             <?= $form->field($model, 'phone')->textInput()  ?>
        </div>
        <div id="div-access" class="col-md-12" style=" ">      
             <?= $form->field($model, 'access')->checkbox()  ?>
        </div>
        <div id="div-pay-amount" class="col-md-12" style=" ">        
             <?= $form->field($model, 'pay_amount')->textInput()  ?>
        </div>
        <div id="div-branch-id" class="col-md-12" style=" ">                
         <?= $form->field($model, 'branch_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'role_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Role::find()->all(), 'id', 'name')) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip']) ?>

    </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

