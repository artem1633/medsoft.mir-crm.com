<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="file-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                          
             <?= $form->field($model, 'folder', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'subfolder', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'size', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'extension', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                      
             <?= $form->field($model, 'datetime', ['cols' => 6, 'colsOptionsStr' => " "])->input('datetime-local')  ?>
                    
             <?= $form->field($model, 'opisanie_sten', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'path', ['cols' => 6, 'colsOptionsStr' => " "])->textInput()  ?>
                    
             <?= $form->field($model, 'name', ['cols' => 12, 'colsOptionsStr' => " "])->textInput()  ?>
                    
         <?= $form->field($model, 'type', ['cols' => 4, 'colsOptionsStr' => " "])->dropDownList(app\models\File::typeLabels(), ['prompt' => 'Выберите вариант']) ?>
      
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

