<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["services"."/".$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
   
 
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
         'visible'=>\app\models\Services::isVisibleAttr('name'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'artikul',
         'visible'=>\app\models\Services::isVisibleAttr('artikul'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kod_804h',
         'visible'=>\app\models\Services::isVisibleAttr('kod_804h'),
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_id',
        'visible'=>\app\models\Services::isVisibleAttr('category_id'),
        'value'=>'category.name',
        'filter'=> ArrayHelper::map(\app\models\ServiceCategory::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'specialnost',
        'visible'=>\app\models\Services::isVisibleAttr('specialnost'),
        'value'=>'specialnost.name',
        'filter'=> ArrayHelper::map(\app\models\Specialization::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'dlitelnost',
        'visible' => \app\models\Services::isVisibleAttr('dlitelnost'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'price',
        'visible' => \app\models\Services::isVisibleAttr('price'),
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],

];   

