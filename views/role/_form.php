<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->doctor_office_disallow_fields = explode(',', $model->doctor_office_disallow_fields);
    $model->patient_disallow_fields = explode(',', $model->patient_disallow_fields);
    $model->help_disallow_fields = explode(',', $model->help_disallow_fields);
    $model->honey_books_disallow_fields = explode(',', $model->honey_books_disallow_fields);
    $model->box_office_disallow_fields = explode(',', $model->box_office_disallow_fields);
    $model->salary_disallow_fields = explode(',', $model->salary_disallow_fields);
    $model->file_disallow_fields = explode(',', $model->file_disallow_fields);
    $model->store_manager_disallow_fields = explode(',', $model->store_manager_disallow_fields);
    $model->store_in_disallow_fields = explode(',', $model->store_in_disallow_fields);
    $model->store_in_item_disallow_fields = explode(',', $model->store_in_item_disallow_fields);
    $model->store_remains_disallow_fields = explode(',', $model->store_remains_disallow_fields);
    $model->store_data_disallow_fields = explode(',', $model->store_data_disallow_fields);
    $model->store_salvage_disallow_fields = explode(',', $model->store_salvage_disallow_fields);
    $model->store_salvage_item_disallow_fields = explode(',', $model->store_salvage_item_disallow_fields);
}

?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Кабинет врача</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'doctor_office_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\DoctorOffice())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Пациент</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'patient_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\Patient())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Справки</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'help_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'help_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\Help())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Мед книжки</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'honey_books_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\HoneyBooks())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Касса</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'box_office_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\BoxOffice())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Зарплата</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'salary_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\Salary())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Файлы</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'file_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'file_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'file_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'file_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'file_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'file_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\File())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Управление складом</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_manager_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreManager())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Поступления</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreIn())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Позиции поступления</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_in_item_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreInItem())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Остатки</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_remains_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreRemains())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Сводные данные</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_data_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreData())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Списание</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreSalvage())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Позиции списания</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_view_all')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'store_salvage_item_disallow_fields')->widget(\kartik\select2\Select2::class, [
                        'data' => \app\helpers\AttributesHelper::roleHandle((new \app\models\StoreSalvageItem())->attributeLabels()),
                        'options' => [
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'books')->checkbox() ?>
        </div>
    </div>

    <?php  if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php  } ?>

    <?php  ActiveForm::end(); ?>
</div>
