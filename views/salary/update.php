<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Salary */
?>
<div class="salary-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
