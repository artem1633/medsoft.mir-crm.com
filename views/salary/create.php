<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Salary */

?>
<div class="salary-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
