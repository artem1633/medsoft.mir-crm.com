<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Salary */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>
<div class="salary-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                              
         <?= $form->field($model, 'user_id', ['cols' => 12, 'colsOptionsStr' => " "])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
                
             <?= $form->field($model, 'amounts', ['cols' => 4, 'colsOptionsStr' => " "])->textInput(['type' => 'number'])  ?>
                    
         <?= $form->field($model, 'way', ['cols' => 12, 'colsOptionsStr' => " "])->dropDownList(app\models\Salary::wayLabels(), ['prompt' => 'Выберите вариант']) ?>
      
                    
         <?= $form->field($model, 'status', ['cols' => 6, 'colsOptionsStr' => " "])->dropDownList(app\models\Salary::statusLabels(), ['prompt' => 'Выберите вариант']) ?>
      
    

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

