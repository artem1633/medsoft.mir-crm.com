<html>
  <head>
    
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;800&display=swap" rel="stylesheet">
<link rel="icon" href="https://klinika-aspirin.ru/wp-content/uploads/2020/12/favicon.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://klinika-aspirin.ru/wp-content/uploads/2020/12/favicon.png" />
<meta name="msapplication-TileImage" content="http://klinika-aspirin.ru/wp-content/uploads/2020/12/favicon.png" />

<style>
* {padding: 0; margin: 0; border: 0;}
body {font-family: 'Montserrat', sans-serif; }
h1,h2,h3,h4 {font-weight: 600;}
h3 {}
h4 {}
.asp1 {width: 65px;float: left;}
.asp2 {width: 70%; float: left;}
header {width: 100%; padding: 15px 0;}
.content {padding: 15px 30px;}
.cnt {margin: 30px 0;}
p {color: #94969c; margin-top: 15px;}
footer {position: absolute; bottom: 0; width: 100%; height: 60px; background: #00a44e;}
hr {height: 1px;}
</style>

  </head>
<body>  	
<?=$content;?>
  </body>
  
  
  <footer></footer>
</html>