<?php

namespace app\controllers;

use Yii;
use app\behaviors\RoleBehavior;
use app\models\StoreManager;
use app\models\StoreManagerSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * StoreManagerController implements the CRUD actions for StoreManager model.
 */
class StoreManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays a single StoreManager model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);





        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Управление складом #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }



    /**
     * Displays a single StoreManager model.
     * 
     * @return mixed
     */
    public function actionIndex()
    {   
        $request = Yii::$app->request;





        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->render('view', [
                    
            ]);
        }else{
            return $this->render('view', [
                    
            ]);
        }
    }
    /**
     * Displays a single StoreManager model.
     * 
     * @return mixed
     */
    public function actionRemains()
    {   
        $request = Yii::$app->request;


            $storeRemainsSearchModel = new \app\models\StoreRemainsSearch();
        $storeRemainsDataProvider = $storeRemainsSearchModel->search(Yii::$app->request->queryParams);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->render('view', [
                    
			'storeRemainsSearchModel'=>$storeRemainsSearchModel,
		'storeRemainsDataProvider'=>$storeRemainsDataProvider,
            ]);
        }else{
            return $this->render('view', [
                    
			'storeRemainsSearchModel'=>$storeRemainsSearchModel,
		'storeRemainsDataProvider'=>$storeRemainsDataProvider,
            ]);
        }
    }
    /**
     * Displays a single StoreManager model.
     * 
     * @return mixed
     */
    public function actionIncome()
    {   
        $request = Yii::$app->request;


            $storeInSearchModel = new \app\models\StoreInSearch();
        $storeInDataProvider = $storeInSearchModel->search(Yii::$app->request->queryParams);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->render('view', [
                    
			'storeInSearchModel'=>$storeInSearchModel,
		'storeInDataProvider'=>$storeInDataProvider,
            ]);
        }else{
            return $this->render('view', [
                    
			'storeInSearchModel'=>$storeInSearchModel,
		'storeInDataProvider'=>$storeInDataProvider,
            ]);
        }
    }
    /**
     * Displays a single StoreManager model.
     * 
     * @return mixed
     */
    public function actionOut()
    {   
        $request = Yii::$app->request;


            $storeSalvageSearchModel = new \app\models\StoreSalvageSearch();
        $storeSalvageDataProvider = $storeSalvageSearchModel->search(Yii::$app->request->queryParams);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->render('view', [
                    
			'storeSalvageSearchModel'=>$storeSalvageSearchModel,
		'storeSalvageDataProvider'=>$storeSalvageDataProvider,
            ]);
        }else{
            return $this->render('view', [
                    
			'storeSalvageSearchModel'=>$storeSalvageSearchModel,
		'storeSalvageDataProvider'=>$storeSalvageDataProvider,
            ]);
        }
    }
    /**
     * Creates a new StoreManager model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new StoreManager();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Управление складом",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-store_manager-pjax',
                    'title'=> "Добавить Управление складом",
                    'content'=>'<span class="text-success">Создание Управление складом успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ]; 
        
            }else{           
                return [
                    'title'=> "Добавить Управление складом",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing StoreManager model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Управление складом #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-store_manager-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Управление складом #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
 
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    
    

    /**
     * Delete an existing StoreManager model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-store_manager-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing StoreManager model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-store_manager-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the StoreManager model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return StoreManager the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StoreManager::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
