<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Patient;

/**
 * PatientSearch represents the model behind the search form about `Patient`.
 */
class PatientSearch extends Patient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'string'],
            [['pol', 'god_rojdeniya', 'posledniy_vizit', 'zaregistrirovan', 'branche_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pol' => $this->pol,
            'branche_id' => $this->branche_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone]);

              if($this->posledniy_vizit){
             $date = explode(' - ', $this->posledniy_vizit);
             $query->andWhere(['between', 'posledniy_vizit', $date[0], $date[1]]);
         }
          if($this->zaregistrirovan){
             $date = explode(' - ', $this->zaregistrirovan);
             $query->andWhere(['between', 'zaregistrirovan', $date[0], $date[1]]);
         }
 
        return $dataProvider;
    }
}
