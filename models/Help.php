<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "help".
*
    * @property string $qr QR
    * @property int $patient_id Пациент
    * @property int $type_id Тип
    * @property  $create_at Дата и время
    * @property int $user_id Сотрудник
    * @property int $agent_id Агент
    * @property  $amounts Сумма
    * @property  $data_vzyatiya_biomateriala Дата взятия биоматериала
    * @property string $vremya_vzyatiya_biomateriala Время взятия биоматериала
    * @property  $data_vydachi Дата выдачи
    * @property string $vremya_vydachi Время выдачи
    * @property int $vrach_id Врач
    * @property string $srok Срок
    * @property int $zaklyuchenie Заключение
    * @property string $kolichestvo Количество
    * @property int $firm_id Фирма
    * @property int $branche_id Филиал
    * @property  $oplata Оплачен
*/
class Help extends \app\base\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'help';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypesCertificates::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agents::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['vrach_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['vrach_id' => 'id']],
            [['zaklyuchenie'], 'exist', 'skipOnError' => true, 'targetClass' => TypesCertificates::className(), 'targetAttribute' => ['zaklyuchenie' => 'id']],
            [['firm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Firm::className(), 'targetAttribute' => ['firm_id' => 'id']],
            [['branche_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branche_id' => 'id']],
            [['qr', 'create_at', 'data_vzyatiya_biomateriala', 'vremya_vzyatiya_biomateriala', 'data_vydachi', 'vremya_vydachi', 'srok', 'kolichestvo'], 'string'],
            [['patient_id', 'type_id', 'user_id', 'agent_id', 'vrach_id', 'zaklyuchenie', 'firm_id', 'branche_id', 'oplata'], 'integer'],
            [['amounts'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'qr' => Yii::t('app', 'QR'),
            'patient_id' => Yii::t('app', 'Пациент'),
            'type_id' => Yii::t('app', 'Тип'),
            'create_at' => Yii::t('app', 'Дата и время'),
            'user_id' => Yii::t('app', 'Сотрудник'),
            'agent_id' => Yii::t('app', 'Агент'),
            'amounts' => Yii::t('app', 'Сумма'),
            'data_vzyatiya_biomateriala' => Yii::t('app', 'Дата взятия биоматериала'),
            'vremya_vzyatiya_biomateriala' => Yii::t('app', 'Время взятия биоматериала'),
            'data_vydachi' => Yii::t('app', 'Дата выдачи'),
            'vremya_vydachi' => Yii::t('app', 'Время выдачи'),
            'vrach_id' => Yii::t('app', 'Врач'),
            'srok' => Yii::t('app', 'Срок'),
            'zaklyuchenie' => Yii::t('app', 'Заключение'),
            'kolichestvo' => Yii::t('app', 'Количество'),
            'firm_id' => Yii::t('app', 'Фирма'),
            'branche_id' => Yii::t('app', 'Филиал'),
            'oplata' => Yii::t('app', 'Оплачен'),
                                                                                                                                                                
];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



                if ($this->isNewRecord){
            $this->user_id = Yii::$app->user->identity->id;
            $this->branche_id = Yii::$app->user->identity->branch_id;
            $this->create_at = date('Y-m-d H:i:s');
            $format = new \Da\QrCode\Format\BookMarkFormat(['title' => '2amigos', 'url' => 'http://medsoft.mir-crm.com/api/check/info?id='.$this->id]);
            $i = "uploads/".Yii::$app->security->generateRandomString().'.png';     
            $qrCode = new \Da\QrCode\QrCode($format);
            $qrCode->writeFile($i); 
            $this->qr = $i;
        }         
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }




    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getType()
    {
        return $this->hasOne(TypesCertificates::className(), ['id' => 'type_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAgent()
    {
        return $this->hasOne(Agents::className(), ['id' => 'agent_id']);
    }

    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getVrach()
    {
        return $this->hasOne(User::className(), ['id' => 'vrach_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getZaklyuchenie()
    {
        return $this->hasOne(TypesCertificates::className(), ['id' => 'zaklyuchenie']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getFirm()
    {
        return $this->hasOne(Firm::className(), ['id' => 'firm_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranche()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branche_id']);
    }

    


    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  

}