<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "patient".
*
    * @property string $name ФИО
    * @property  $pol Пол
    * @property  $god_rojdeniya Год рождения
    * @property string $phone Телефон
    * @property  $posledniy_vizit Последний визит
    * @property  $zaregistrirovan Зарегистрирован
    * @property int $branche_id Филиал
*/
class Patient extends \app\base\ActiveRecord
{

        const V10 = 'М';
    const V11 = 'Ж';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'patient';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['branche_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::className(), 'targetAttribute' => ['branche_id' => 'id']],
            [['name', 'pol', 'god_rojdeniya', 'phone', 'posledniy_vizit', 'zaregistrirovan'], 'string'],
            [['branche_id'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'ФИО'),
            'pol' => Yii::t('app', 'Пол'),
            'god_rojdeniya' => Yii::t('app', 'Год рождения'),
            'phone' => Yii::t('app', 'Телефон'),
            'posledniy_vizit' => Yii::t('app', 'Последний визит'),
            'zaregistrirovan' => Yii::t('app', 'Зарегистрирован'),
            'branche_id' => Yii::t('app', 'Филиал'),
                                                                        
];
    }

    public static function polLabels() {
        return [
            self::V10 => "М",
            self::V11 => "Ж",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }




    
    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBranche()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branche_id']);
    }



    public function tags($text)
    {
        $arr = [];
        $result = preg_match_all('/\{.*?}/',$text,$arr);
        foreach ($arr[0] as $value) {
            $value2 = str_replace('{', '', $value);
            $value2 = str_replace('}', '', $value2);
            $text = str_replace($value, yii\helpers\ArrayHelper::getValue($this, $value2), $text);
        }

        return $text;
    }  
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHelps()
    {
        return $this->hasMany(Help::className(), ['patient_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBoxOffices()
    {
        return $this->hasMany(BoxOffice::className(), ['patient_id' => 'id']);
    }

}