<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StoreIn;

/**
 * StoreInSearch represents the model behind the search form about `StoreIn`.
 */
class StoreInSearch extends StoreIn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomer_zakaza', 'vkladka'], 'string'],
            [['warehouse_id', 'status', 'create_at', 'the_supplier_id', 'position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreIn::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'warehouse_id' => $this->warehouse_id,
            'status' => $this->status,
            'the_supplier_id' => $this->the_supplier_id,
        ]);

        $query->andFilterWhere(['like', 'nomer_zakaza', $this->nomer_zakaza])
            ->andFilterWhere(['like', 'vkladka', $this->vkladka]);

             if($this->create_at){
             $date = explode(' - ', $this->create_at);
             $query->andWhere(['between', 'create_at', $date[0], $date[1]]);
         }
   
        return $dataProvider;
    }
}
