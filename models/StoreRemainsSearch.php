<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StoreRemains;

/**
 * StoreRemainsSearch represents the model behind the search form about `StoreRemains`.
 */
class StoreRemainsSearch extends StoreRemains
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vkladka', 'price'], 'string'],
            [['warehouse_id', 'tovar_id', 'amount', 'mesto_na_sklade'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreRemains::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'warehouse_id' => $this->warehouse_id,
            'tovar_id' => $this->tovar_id,
            'mesto_na_sklade' => $this->mesto_na_sklade,
        ]);

        $query->andFilterWhere(['like', 'vkladka', $this->vkladka])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'price', $this->price]);

      
        return $dataProvider;
    }
}
