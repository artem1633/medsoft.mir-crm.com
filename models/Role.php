<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $doctor_office_create Кабинет врача Создание
 * @property integer $doctor_office_update Кабинет врача Изменение
 * @property integer $doctor_office_view Кабинет врача Просмотр
 * @property integer $doctor_office_view_all Кабинет врача Просмотр всех
 * @property integer $doctor_office_delete Кабинет врача Удаление
 * @property integer $doctor_office_disallow_fields Кабинет врача Исключенные поля
 * @property integer $patient_create Пациент Создание
 * @property integer $patient_update Пациент Изменение
 * @property integer $patient_view Пациент Просмотр
 * @property integer $patient_view_all Пациент Просмотр всех
 * @property integer $patient_delete Пациент Удаление
 * @property integer $patient_disallow_fields Пациент Исключенные поля
 * @property integer $help_create Справки Создание
 * @property integer $help_update Справки Изменение
 * @property integer $help_view Справки Просмотр
 * @property integer $help_view_all Справки Просмотр всех
 * @property integer $help_delete Справки Удаление
 * @property integer $help_disallow_fields Справки Исключенные поля
 * @property integer $honey_books_create Мед книжки Создание
 * @property integer $honey_books_update Мед книжки Изменение
 * @property integer $honey_books_view Мед книжки Просмотр
 * @property integer $honey_books_view_all Мед книжки Просмотр всех
 * @property integer $honey_books_delete Мед книжки Удаление
 * @property integer $honey_books_disallow_fields Мед книжки Исключенные поля
 * @property integer $box_office_create Касса Создание
 * @property integer $box_office_update Касса Изменение
 * @property integer $box_office_view Касса Просмотр
 * @property integer $box_office_view_all Касса Просмотр всех
 * @property integer $box_office_delete Касса Удаление
 * @property integer $box_office_disallow_fields Касса Исключенные поля
 * @property integer $salary_create Зарплата Создание
 * @property integer $salary_update Зарплата Изменение
 * @property integer $salary_view Зарплата Просмотр
 * @property integer $salary_view_all Зарплата Просмотр всех
 * @property integer $salary_delete Зарплата Удаление
 * @property integer $salary_disallow_fields Зарплата Исключенные поля
 * @property integer $file_create Файлы Создание
 * @property integer $file_update Файлы Изменение
 * @property integer $file_view Файлы Просмотр
 * @property integer $file_view_all Файлы Просмотр всех
 * @property integer $file_delete Файлы Удаление
 * @property integer $file_disallow_fields Файлы Исключенные поля
 * @property integer $store_manager_create Управление складом Создание
 * @property integer $store_manager_update Управление складом Изменение
 * @property integer $store_manager_view Управление складом Просмотр
 * @property integer $store_manager_view_all Управление складом Просмотр всех
 * @property integer $store_manager_delete Управление складом Удаление
 * @property integer $store_manager_disallow_fields Управление складом Исключенные поля
 * @property integer $store_in_create Поступления Создание
 * @property integer $store_in_update Поступления Изменение
 * @property integer $store_in_view Поступления Просмотр
 * @property integer $store_in_view_all Поступления Просмотр всех
 * @property integer $store_in_delete Поступления Удаление
 * @property integer $store_in_disallow_fields Поступления Исключенные поля
 * @property integer $store_in_item_create Позиции поступления Создание
 * @property integer $store_in_item_update Позиции поступления Изменение
 * @property integer $store_in_item_view Позиции поступления Просмотр
 * @property integer $store_in_item_view_all Позиции поступления Просмотр всех
 * @property integer $store_in_item_delete Позиции поступления Удаление
 * @property integer $store_in_item_disallow_fields Позиции поступления Исключенные поля
 * @property integer $store_remains_create Остатки Создание
 * @property integer $store_remains_update Остатки Изменение
 * @property integer $store_remains_view Остатки Просмотр
 * @property integer $store_remains_view_all Остатки Просмотр всех
 * @property integer $store_remains_delete Остатки Удаление
 * @property integer $store_remains_disallow_fields Остатки Исключенные поля
 * @property integer $store_data_create Сводные данные Создание
 * @property integer $store_data_update Сводные данные Изменение
 * @property integer $store_data_view Сводные данные Просмотр
 * @property integer $store_data_view_all Сводные данные Просмотр всех
 * @property integer $store_data_delete Сводные данные Удаление
 * @property integer $store_data_disallow_fields Сводные данные Исключенные поля
 * @property integer $store_salvage_create Списание Создание
 * @property integer $store_salvage_update Списание Изменение
 * @property integer $store_salvage_view Списание Просмотр
 * @property integer $store_salvage_view_all Списание Просмотр всех
 * @property integer $store_salvage_delete Списание Удаление
 * @property integer $store_salvage_disallow_fields Списание Исключенные поля
 * @property integer $store_salvage_item_create Позиции списания Создание
 * @property integer $store_salvage_item_update Позиции списания Изменение
 * @property integer $store_salvage_item_view Позиции списания Просмотр
 * @property integer $store_salvage_item_view_all Позиции списания Просмотр всех
 * @property integer $store_salvage_item_delete Позиции списания Удаление
 * @property integer $store_salvage_item_disallow_fields Позиции списания Исключенные поля
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doctor_office_create', 'doctor_office_update', 'doctor_office_view', 'doctor_office_view_all', 'doctor_office_delete', 'patient_create', 'patient_update', 'patient_view', 'patient_view_all', 'patient_delete', 'help_create', 'help_update', 'help_view', 'help_view_all', 'help_delete', 'honey_books_create', 'honey_books_update', 'honey_books_view', 'honey_books_view_all', 'honey_books_delete', 'box_office_create', 'box_office_update', 'box_office_view', 'box_office_view_all', 'box_office_delete', 'salary_create', 'salary_update', 'salary_view', 'salary_view_all', 'salary_delete', 'file_create', 'file_update', 'file_view', 'file_view_all', 'file_delete', 'store_manager_create', 'store_manager_update', 'store_manager_view', 'store_manager_view_all', 'store_manager_delete', 'store_in_create', 'store_in_update', 'store_in_view', 'store_in_view_all', 'store_in_delete', 'store_in_item_create', 'store_in_item_update', 'store_in_item_view', 'store_in_item_view_all', 'store_in_item_delete', 'store_remains_create', 'store_remains_update', 'store_remains_view', 'store_remains_view_all', 'store_remains_delete', 'store_data_create', 'store_data_update', 'store_data_view', 'store_data_view_all', 'store_data_delete', 'store_salvage_create', 'store_salvage_update', 'store_salvage_view', 'store_salvage_view_all', 'store_salvage_delete', 'store_salvage_item_create', 'store_salvage_item_update', 'store_salvage_item_view', 'store_salvage_item_view_all', 'store_salvage_item_delete', 'books'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
            [['doctor_office_disallow_fields', 'patient_disallow_fields', 'help_disallow_fields', 'honey_books_disallow_fields', 'box_office_disallow_fields', 'salary_disallow_fields', 'file_disallow_fields', 'store_manager_disallow_fields', 'store_in_disallow_fields', 'store_in_item_disallow_fields', 'store_remains_disallow_fields', 'store_data_disallow_fields', 'store_salvage_disallow_fields', 'store_salvage_item_disallow_fields'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Роль',
            'doctor_office_create' => 'Создание',
            'doctor_office_update' => 'Изменение',
            'doctor_office_view' => 'Просмотр',
            'doctor_office_view_all' => 'Просмотр всех',
            'doctor_office_delete' => 'Удаление',
            'patient_create' => 'Создание',
            'patient_update' => 'Изменение',
            'patient_view' => 'Просмотр',
            'patient_view_all' => 'Просмотр всех',
            'patient_delete' => 'Удаление',
            'help_create' => 'Создание',
            'help_update' => 'Изменение',
            'help_view' => 'Просмотр',
            'help_view_all' => 'Просмотр всех',
            'help_delete' => 'Удаление',
            'honey_books_create' => 'Создание',
            'honey_books_update' => 'Изменение',
            'honey_books_view' => 'Просмотр',
            'honey_books_view_all' => 'Просмотр всех',
            'honey_books_delete' => 'Удаление',
            'box_office_create' => 'Создание',
            'box_office_update' => 'Изменение',
            'box_office_view' => 'Просмотр',
            'box_office_view_all' => 'Просмотр всех',
            'box_office_delete' => 'Удаление',
            'salary_create' => 'Создание',
            'salary_update' => 'Изменение',
            'salary_view' => 'Просмотр',
            'salary_view_all' => 'Просмотр всех',
            'salary_delete' => 'Удаление',
            'file_create' => 'Создание',
            'file_update' => 'Изменение',
            'file_view' => 'Просмотр',
            'file_view_all' => 'Просмотр всех',
            'file_delete' => 'Удаление',
            'store_manager_create' => 'Создание',
            'store_manager_update' => 'Изменение',
            'store_manager_view' => 'Просмотр',
            'store_manager_view_all' => 'Просмотр всех',
            'store_manager_delete' => 'Удаление',
            'store_in_create' => 'Создание',
            'store_in_update' => 'Изменение',
            'store_in_view' => 'Просмотр',
            'store_in_view_all' => 'Просмотр всех',
            'store_in_delete' => 'Удаление',
            'store_in_item_create' => 'Создание',
            'store_in_item_update' => 'Изменение',
            'store_in_item_view' => 'Просмотр',
            'store_in_item_view_all' => 'Просмотр всех',
            'store_in_item_delete' => 'Удаление',
            'store_remains_create' => 'Создание',
            'store_remains_update' => 'Изменение',
            'store_remains_view' => 'Просмотр',
            'store_remains_view_all' => 'Просмотр всех',
            'store_remains_delete' => 'Удаление',
            'store_data_create' => 'Создание',
            'store_data_update' => 'Изменение',
            'store_data_view' => 'Просмотр',
            'store_data_view_all' => 'Просмотр всех',
            'store_data_delete' => 'Удаление',
            'store_salvage_create' => 'Создание',
            'store_salvage_update' => 'Изменение',
            'store_salvage_view' => 'Просмотр',
            'store_salvage_view_all' => 'Просмотр всех',
            'store_salvage_delete' => 'Удаление',
            'store_salvage_item_create' => 'Создание',
            'store_salvage_item_update' => 'Изменение',
            'store_salvage_item_view' => 'Просмотр',
            'store_salvage_item_view_all' => 'Просмотр всех',
            'store_salvage_item_delete' => 'Удаление',
                    'doctor_office_disallow_fields' => 'Исключенные поля',
            'patient_disallow_fields' => 'Исключенные поля',
            'help_disallow_fields' => 'Исключенные поля',
            'honey_books_disallow_fields' => 'Исключенные поля',
            'box_office_disallow_fields' => 'Исключенные поля',
            'salary_disallow_fields' => 'Исключенные поля',
            'file_disallow_fields' => 'Исключенные поля',
            'store_manager_disallow_fields' => 'Исключенные поля',
            'store_in_disallow_fields' => 'Исключенные поля',
            'store_in_item_disallow_fields' => 'Исключенные поля',
            'store_remains_disallow_fields' => 'Исключенные поля',
            'store_data_disallow_fields' => 'Исключенные поля',
            'store_salvage_disallow_fields' => 'Исключенные поля',
            'store_salvage_item_disallow_fields' => 'Исключенные поля',
            'books' => 'Справочники'
];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {
        if($this->doctor_office_disallow_fields && is_array($this->doctor_office_disallow_fields)){
            $this->doctor_office_disallow_fields = implode(',', $this->doctor_office_disallow_fields);
        }
        if($this->patient_disallow_fields && is_array($this->patient_disallow_fields)){
            $this->patient_disallow_fields = implode(',', $this->patient_disallow_fields);
        }
        if($this->help_disallow_fields && is_array($this->help_disallow_fields)){
            $this->help_disallow_fields = implode(',', $this->help_disallow_fields);
        }
        if($this->honey_books_disallow_fields && is_array($this->honey_books_disallow_fields)){
            $this->honey_books_disallow_fields = implode(',', $this->honey_books_disallow_fields);
        }
        if($this->box_office_disallow_fields && is_array($this->box_office_disallow_fields)){
            $this->box_office_disallow_fields = implode(',', $this->box_office_disallow_fields);
        }
        if($this->salary_disallow_fields && is_array($this->salary_disallow_fields)){
            $this->salary_disallow_fields = implode(',', $this->salary_disallow_fields);
        }
        if($this->file_disallow_fields && is_array($this->file_disallow_fields)){
            $this->file_disallow_fields = implode(',', $this->file_disallow_fields);
        }
        if($this->store_manager_disallow_fields && is_array($this->store_manager_disallow_fields)){
            $this->store_manager_disallow_fields = implode(',', $this->store_manager_disallow_fields);
        }
        if($this->store_in_disallow_fields && is_array($this->store_in_disallow_fields)){
            $this->store_in_disallow_fields = implode(',', $this->store_in_disallow_fields);
        }
        if($this->store_in_item_disallow_fields && is_array($this->store_in_item_disallow_fields)){
            $this->store_in_item_disallow_fields = implode(',', $this->store_in_item_disallow_fields);
        }
        if($this->store_remains_disallow_fields && is_array($this->store_remains_disallow_fields)){
            $this->store_remains_disallow_fields = implode(',', $this->store_remains_disallow_fields);
        }
        if($this->store_data_disallow_fields && is_array($this->store_data_disallow_fields)){
            $this->store_data_disallow_fields = implode(',', $this->store_data_disallow_fields);
        }
        if($this->store_salvage_disallow_fields && is_array($this->store_salvage_disallow_fields)){
            $this->store_salvage_disallow_fields = implode(',', $this->store_salvage_disallow_fields);
        }
        if($this->store_salvage_item_disallow_fields && is_array($this->store_salvage_item_disallow_fields)){
            $this->store_salvage_item_disallow_fields = implode(',', $this->store_salvage_item_disallow_fields);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
